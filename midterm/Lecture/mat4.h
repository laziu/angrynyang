#pragma once
#include "vec3.h"
struct mat4
{
	float p[16];

	float& operator[](int i);
	operator const float *() const;
	
	mat4& operator=(const mat4& m);
	mat4 operator*(const mat4& m) const;

	static mat4 identity();
	static mat4 rotation(float angle, const vec3& axis);
	static mat4 transform(const vec3& move);
};
