#include "mat4.h"
#include <cmath>

// index of matrix, [row][col]
inline int mdx(int i, int j) {
	return i + 4*j;
}

// a[i]
float & mat4::operator[](int i) {
	return p[i];
}

// cast to float array
mat4::operator const float*() const {
	return p;
}

// a = b
mat4 & mat4::operator=(const mat4 & m) {
	for (int i = 0; i < 16; ++i)
		p[i] = m.p[i];
	return *this;
}

// a * b
mat4 mat4::operator*(const mat4 & m) const {
	mat4 a = {};
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			for (int k = 0; k < 4; ++k)
				a.p[mdx(i, j)] += p[mdx(i, k)] * m.p[mdx(k, j)];
	return a;
}

// identity matrix
mat4 mat4::identity() {
	return {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
}

// rotation matrix
mat4 mat4::rotation(float a, const vec3 & axis) {
	vec3 v = axis.normalized();
	mat4 m = mat4::identity();

	m.p[mdx(0, 0)] = cosf(a) + v[0] * v[0] * (1.0f - cosf(a));
	m.p[mdx(0, 1)] = v[0] * v[1] * (1.0f - cosf(a)) - v[2] * sinf(a);
	m.p[mdx(0, 2)] = v[0] * v[2] * (1.0f - cosf(a)) + v[1] * sinf(a);
	m.p[mdx(1, 0)] = v[1] * v[0] * (1.0f - cosf(a)) + v[2] * sinf(a);
	m.p[mdx(1, 1)] = cosf(a) + v[1] * v[1] * (1.0f - cosf(a));
	m.p[mdx(1, 2)] = v[1] * v[2] * (1.0f - cosf(a)) - v[0] * sinf(a);
	m.p[mdx(2, 0)] = v[2] * v[0] * (1.0f - cosf(a)) - v[1] * sinf(a);
	m.p[mdx(2, 1)] = v[2] * v[1] * (1.0f - cosf(a)) + v[0] * sinf(a);
	m.p[mdx(2, 2)] = cosf(a) + v[2] * v[2] * (1.0f - cosf(a));

	return m;
}

// transform matrix
mat4 mat4::transform(const vec3 & v) {
	mat4 m = mat4::identity();
	m.p[mdx(0, 3)] = v[0];
	m.p[mdx(1, 3)] = v[1];
	m.p[mdx(2, 3)] = v[2];
	return m;
}
