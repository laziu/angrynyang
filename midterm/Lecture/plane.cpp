#include "plane.h"

using namespace planeindex;

vec3 & plane::operator[](int i) {
	return p[i];
}

plane::operator const vec3*() const {
	return p;
}

vec3 plane::centroid() {
	return (p[a] + p[b] + p[c]) / 3.0f;
}

vec3 plane::normal() {
	return ((p[b] - p[a]) * (p[c] - p[a])).normalized();
}
