#include <cstdio>
#include <Windows.h>
#include <iostream>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "problem_list.h"

using namespace std;

void Render(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// call problem's function
	if (!problem_initialized) {
		system("cls");
		puts("press 1 ~ 6 key to switch problem");
		printf("---------- problem %d ----------\n\n", problem_number);
		problem_init[problem_number]();
		problem_initialized = 1;
	}
	problem_draw[problem_number]();

	glFlush();

	glutSwapBuffers();
}

void Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (float)w / (float)h, 1.0f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void Mouse(int mouse_event, int state, int x, int y)
{
	switch(mouse_event)
	{
		case GLUT_LEFT_BUTTON:
		
			break;
		case GLUT_MIDDLE_BUTTON:
		
			break;
		case GLUT_RIGHT_BUTTON:
		
			break;
		default:
			break;		
	}
	glutPostRedisplay();
}


void Motion(int x, int y)
{
	
	glutPostRedisplay();
}


void Keyboard(unsigned char key, int x, int y)
{
	switch(key)
	{
		// press 1 ~ 5 to switch problem number
		// set problem_number; problem_initialized = false;
		case '1': problem_number = 1; problem_initialized = 0; break;
		case '2': problem_number = 2; problem_initialized = 0; break;
		case '3': problem_number = 3; problem_initialized = 0; break;
		case '4': problem_number = 4; problem_initialized = 0; break;
		case '5': problem_number = 5; problem_initialized = 0; break;
		case '6': problem_number = 6; problem_initialized = 0; break;
		case VK_ESCAPE:
			exit(0);
		break;
		default:
			if (problem_keyEvent[problem_number])
				problem_keyEvent[problem_number](key);
		break;
	}
	glutPostRedisplay();
}

void SpecialKey(int key, int x, int y)
{
	switch(key) 
	{
		case GLUT_KEY_F1:
			printf("Hellow World \n");
			break;
	}
	glutPostRedisplay();
}

void Idle(void)
{	
	glutPostRedisplay();
}

int main(int argc, char ** argv)
{
	glutInit(&argc, argv);
	
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	
	glutInitWindowSize(400,400);
	glutInitWindowPosition(0, 0);

	//Console Window 위치 변경
	HWND hWnd = ::FindWindow("ConsoleWindowClass" ,NULL );
	::MoveWindow(hWnd , 420 , 0 , 500 , 500, TRUE );

	//Window 제목은 학번과 학생 이름으로 작성하십시오.
	glutCreateWindow("2015410116_김건우");

	glEnable(GL_DEPTH_TEST);

	glutDisplayFunc(Render);
	glutReshapeFunc(Reshape);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SpecialKey);
	glutIdleFunc(Idle);
	glutMainLoop();

	return 0;
}

