#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vec3.h"
#include "model.h"

namespace
{
	model m;

	// random position
	vec3 point;

	// face relation with point
	int relation[4];

	// rotate angle
	float angle;

	// return random float between -1.0f ~ 1.0f
	float random() {
		int r = rand() * rand();
		r = (r % 10001) - 5000;
		return ((float)r) / 5000.0f;
	}
}

void problem4_init() {
	puts("  positive face: red");
	puts("  negative face: blue");

	// generate random position
	srand(time(0));
	point = { random(), random(), random() };
	point = point.normalized() * 1.5f;

	// solve each face relation with point
	for (int i = 0; i < 4; ++i) {
		// get face's normal vector
		vec3 v1 = m.face(i)[1] - m.face(i)[0];
		vec3 v2 = m.face(i)[2] - m.face(i)[0];
		vec3 n = (v1 * v2).normalized();
		// get vector: face's centroid -> point
		vec3 c = (m.face(i)[0] + m.face(i)[1] + m.face(i)[2]) / 3;
		vec3 p = (point - c).normalized();
		// get dot product of two vector
		float dot = p.dot(c);
		// set relation
		relation[i] = dot > 0;
	}

	angle = 0.0f;
	glLineWidth(1.0f);
	glPointSize(5.0f);
}

void problem4_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// rotate model to view well
	angle += 0.01f;
	glRotatef(angle, 0.3f, 0.7f, 1.0f);

	// draw faces
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < 4; ++i) {
		if (relation[i])
			glColor3f(1.0f, 0.5f, 0.5f); // positive: red
		else
			glColor3f(0.5f, 0.5f, 1.0f); // negative: blue
		glVertex3fv(m.face(i)[0]);
		glVertex3fv(m.face(i)[1]);
		glVertex3fv(m.face(i)[2]);
	}
	glEnd();

	// draw edges of faces
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 3; ++j) {
			glVertex3fv(m.face(i)[j]);
			glVertex3fv(m.face(i)[(j + 1) % 3]);
		}
	}
	glEnd();

	// draw point
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3fv(point);
	glEnd();
}
