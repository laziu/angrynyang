#pragma once
#include "vec3.h"

namespace lineindex
{
	enum { a = 0, b = 1 };
}

struct line
{
	vec3 p[2];
	vec3& operator[](int i);
	operator const vec3 *() const;

	vec3 midium();
};

