#include "problem_list.h"

// each real functions of programs
void problem1_init();
void problem2_init();
void problem3_init();
void problem4_init();
void problem5_init();
void problem6_init();

void problem1_draw();
void problem2_draw();
void problem3_draw();
void problem4_draw();
void problem5_draw();
void problem6_draw();

void problem5_keyEvent(unsigned char key);
void problem6_keyEvent(unsigned char key);

// default problem number = 1
int problem_number = 1;

int problem_initialized = 0;

// set pointer of each problem's function
void(*problem_init[7])() = {
	0, 
	problem1_init,
	problem2_init,
	problem3_init,
	problem4_init,
	problem5_init,
	problem6_init
};

void(*problem_draw[7])() = {
	0,
	problem1_draw,
	problem2_draw,
	problem3_draw,
	problem4_draw,
	problem5_draw,
	problem6_draw
};

void(*problem_keyEvent[7])(unsigned char key) = {
	0,
	0, // 1
	0, // 2
	0, // 3
	0, // 4
	problem5_keyEvent, // 5
	problem6_keyEvent  // 6
};