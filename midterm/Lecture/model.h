#pragma once
#include <string>
#include "vec3.h"
#include "line.h"
#include "plane.h"

struct model
{
private:
	// model data
	vec3 const v_data[4];
	// edges index
	int const e_i[6][2];
	// faces index
	int const f_i[4][3];
public:
	model();
	line edge(int i);
	plane face(int i);
};

// color data list 
extern vec3 const color[6];
// color name list
extern std::string const name[6];