#include "vec3.h"

using namespace vec3index;

// a[i]
float& vec3::operator[](int i) {
	return p[i];
}

// cast to float array
vec3::operator const float *() const {
	return p;
}

// a = b
vec3& vec3::operator=(const vec3& v) {
	p[x] = v[x], p[y] = v[y], p[z] = v[z];
	return *this;
}

// a + b
vec3 vec3::operator+(const vec3& v) const {
	return{ p[x] + v[x], p[y] + v[y], p[z] + v[z] };
}

// a - b
vec3 vec3::operator-(const vec3& v) const {
	return{ p[x] - v[x], p[y] - v[y], p[z] - v[z] };
}

// -a
vec3 vec3::operator-() const {
	return{ -p[x], -p[y], -p[z] };
}

// a.dot(b)
float vec3::dot(const vec3& v) const {
	return p[x] * v[x] + p[y] * v[y] + p[z] * v[z];
}

// a *(cross) b 
vec3 vec3::operator*(const vec3& v) const {
	return{
		p[y] * v[z] - p[z] * v[y], 
		p[z] * v[x] - p[x] * v[z], 
		p[x] * v[y] - p[y] * v[x] 
	};
}

// a.length()
float vec3::length() const {
	return sqrtf(p[x] * p[x] + p[y] * p[y] + p[z] * p[z]);
}

// a * n
vec3 operator*(const vec3& a, float n) {
	return{ a[x] * n, a[y] * n, a[z] * n };
}

// a / n
vec3 operator/(const vec3& a, float n) {
	return{ a[x] / n, a[y] / n, a[z] / n };
}

// a.normalized()
vec3 vec3::normalized() const {
	return (*this) / length();
}