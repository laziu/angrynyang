#include <cmath>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "model.h"

namespace
{
	model m;
	// rotate angle
	float angle;
}

void problem1_init() {
	angle = 0.0f;
}

void problem1_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// rotate model to view well
	angle += 0.01f;
	glRotatef(angle, 0.3f, 0.7f, 1.0f);

	// draw faces
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < 4; ++i) {
		glColor3fv(color[i]); // set color of each faces
		glVertex3fv(m.face(i)[0]);
		glVertex3fv(m.face(i)[1]);
		glVertex3fv(m.face(i)[2]);
	}
	glEnd();
}
