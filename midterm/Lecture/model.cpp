#include "model.h"

model::model(): 
	v_data {
		{ 1.0f              ,  0.0f              ,  0.0f },
		{ -0.333333333333333f,  0.942809041582063f,  0.0f },
		{ -0.333333333333333f, -0.471404520791031f,  0.816496580927727f },
		{ -0.333333333333333f, -0.471404520791031f, -0.816496580927727f }
	},
	e_i	{
		{ 0, 1 },
		{ 0, 2 },
		{ 0, 3 },
		{ 1, 2 },
		{ 2, 3 },
		{ 3, 1 }
	},
	f_i {
		{ 0, 1, 2 },
		{ 0, 2, 3 },
		{ 0, 3, 1 },
		{ 1, 3, 2 }
	}
{}

line model::edge(int i) {
	return { v_data[ e_i[i][0] ], v_data[ e_i[i][1] ] };
}

plane model::face(int i) {
	return { v_data[ f_i[i][0] ], v_data[ f_i[i][1] ], v_data[ f_i[i][2] ] };
}

vec3 const color[6] = {
	{ 1.0f, 0.0f, 0.0f }, // red
	{ 0.0f, 1.0f, 0.0f }, // green
	{ 0.0f, 0.0f, 1.0f }, // blue
	{ 1.0f, 1.0f, 0.0f }, // yellow
	{ 0.0f, 1.0f, 1.0f }, // cyan
	{ 1.0f, 0.0f, 1.0f }  // purple
};

std::string const name[6] = {
	"red"   ,
	"green" ,
	"blue"  ,
	"yellow",
	"cyan"  ,
	"purple"
};

