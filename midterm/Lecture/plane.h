#pragma once
#include "vec3.h"

namespace planeindex
{
	enum { a = 0, b = 1, c = 2 };
}

struct plane
{
	vec3 p[3];
	vec3& operator[](int i);
	operator const vec3 *() const;

	vec3 centroid();
	vec3 normal();
};

