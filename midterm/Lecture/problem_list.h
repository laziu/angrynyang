#pragma once
// problem_number: 1 ~ 6
extern int problem_number;
// problem_initialized: true | false
extern int problem_initialized;

// init and draw function of problems 
extern void(*problem_init[7])();
extern void(*problem_draw[7])();

// key event function of problems, not fully initialized
extern void(*problem_keyEvent[7])(unsigned char key);