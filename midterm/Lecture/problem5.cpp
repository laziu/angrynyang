#include <cstdio>
#include <cmath>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vec3.h"
#include "mat4.h"
#include "model.h"

namespace
{
	model m;
	// rotate matrix
	mat4 matrix;
}

void problem5_init() {
	puts(" rotate +x: Q");
	puts(" rotate -x: E");
	puts(" rotate +y: D");
	puts(" rotate -y: A");
	puts(" rotate +Z: W");
	puts(" rotate -Z: S");

	matrix = mat4::identity();
	glLineWidth(2.0f);
}

void problem5_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// draw axis
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(2.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 2.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 2.0f);
	glEnd();

	// draw faces
	glPushMatrix();
		// multiply rotation matrix
		glMultMatrixf(matrix);

		glBegin(GL_TRIANGLES);
		for (int i = 0; i < 4; ++i) {
			glColor3fv(color[i]); // set color of each faces
			glVertex3fv(m.face(i)[0]);
			glVertex3fv(m.face(i)[1]);
			glVertex3fv(m.face(i)[2]);
		}
		glEnd();
	glPopMatrix();
}

void problem5_keyEvent(unsigned char key) {
	mat4 rotation;
	switch (key) {
	case 'q': rotation = mat4::rotation( 0.05f, {1.0f, 0.0f, 0.0f}); break;
	case 'e': rotation = mat4::rotation(-0.05f, {1.0f, 0.0f, 0.0f}); break;
	case 'd': rotation = mat4::rotation( 0.05f, {0.0f, 1.0f, 0.0f}); break;
	case 'a': rotation = mat4::rotation(-0.05f, {0.0f, 1.0f, 0.0f}); break;
	case 'w': rotation = mat4::rotation( 0.05f, {0.0f, 0.0f, 1.0f}); break;
	case 's': rotation = mat4::rotation(-0.05f, {0.0f, 0.0f, 1.0f}); break;
	default: return;
	}
	matrix = rotation * matrix;
}