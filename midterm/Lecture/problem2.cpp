#include <cmath>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vec3.h"
#include "model.h"

namespace
{
	model m;

	// two points of normal vector of each faces
	vec3 normal[4][2];

	// rotate angle
	float angle;
}

void problem2_init() {
	// calculate position and vector of each normal
	for (int i = 0; i < 4; i++) {
		// start position of normal is centroid of face
		normal[i][0] = m.face(i).centroid();
		// end position of normal = start position + normal vector
		normal[i][1] = normal[i][0] + m.face(i).normal();
	}

	angle = 0.0f;

	glLineWidth(1.0f);
}

void problem2_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// rotate model to view well
	angle += 0.01f;
	glRotatef(angle, 0.3f, 0.7f, 1.0f);

	// draw faces
	glBegin(GL_TRIANGLES);
	glColor3f(0.5f, 0.6f, 0.7f);
	for (int i = 0; i < 4; ++i) {
		glVertex3fv(m.face(i)[0]);
		glVertex3fv(m.face(i)[1]);
		glVertex3fv(m.face(i)[2]);
	}
	glEnd();

	// draw edges of faces
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 3; ++j) {
			glVertex3fv(m.face(i)[ j         ]);
			glVertex3fv(m.face(i)[(j + 1) % 3]);
		}
	}
	glEnd();

	// draw normals
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < 4; ++i) {
		glVertex3fv(normal[i][0]);
		glVertex3fv(normal[i][1]);
	}
	glEnd();
}
