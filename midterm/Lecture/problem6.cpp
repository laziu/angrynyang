#include <cstdio>
#include <cmath>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vec3.h"
#include "mat4.h"
#include "model.h"

namespace
{
	model m;
	// rotate matrix
	mat4 matrix;
}

void problem6_init() {
	puts("    rotation");
	puts("    red  green  blue yellow cyan purple");
	puts(" +   Q     W     E     R     T     Y");
	puts(" -   A     S     D     F     G     H");

	matrix = mat4::identity();
	glLineWidth(2.0f);
}

void problem6_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// draw edges 
	glPushMatrix();
		// multiply rotation matrix
		glMultMatrixf(matrix);

		glLineWidth(5.0f);
		glBegin(GL_LINES);
		for (int i = 0; i < 6; ++i) {
			glColor3fv(color[i]);
			glVertex3fv(m.edge(i)[0]);
			glColor3f(0.05f, 0.05f, 0.05f);
			glVertex3fv(m.edge(i)[1]);
		}
		glEnd();
	glPopMatrix();
}

void problem6_keyEvent(unsigned char key) {
	int v_num;
	float angle;
	switch (key) {
	case 'q': v_num = 0; angle =  0.05f; break;
	case 'w': v_num = 1; angle =  0.05f; break;
	case 'e': v_num = 2; angle =  0.05f; break;
	case 'r': v_num = 3; angle =  0.05f; break;
	case 't': v_num = 4; angle =  0.05f; break;
	case 'y': v_num = 5; angle =  0.05f; break;
	case 'a': v_num = 0; angle = -0.05f; break;
	case 's': v_num = 1; angle = -0.05f; break;
	case 'd': v_num = 2; angle = -0.05f; break;
	case 'f': v_num = 3; angle = -0.05f; break;
	case 'g': v_num = 4; angle = -0.05f; break;
	case 'h': v_num = 5; angle = -0.05f; break;
	default: return;
	}

	vec3 position = m.edge(v_num)[1];
	matrix = matrix 
		* mat4::transform(position)
		* mat4::rotation(angle, m.edge(v_num)[1] - m.edge(v_num)[0])
		* mat4::transform(-position);
}