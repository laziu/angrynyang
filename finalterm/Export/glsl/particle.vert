#version 330 core

layout(location = 0) in vec2 vTexCoord;
layout(location = 1) in vec4 vPosSize;
layout(location = 2) in float vLife;

out vec2 fTexCoord;
out float fAlpha;

uniform vec3 CameraRight;
uniform vec3 CameraUp;
uniform mat4 VP;

void main() {
  float size = vPosSize.w * vLife;
  vec3 center = vPosSize.xyz;

  vec3 position = center + size * ( CameraRight * vTexCoord.x
                                  + CameraUp    * vTexCoord.y );

  gl_Position = VP * vec4(position, 1.0);

  float angle = vPosSize.x + vPosSize.y + vPosSize.z + vPosSize.w;
  mat2 rotation = mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
  fTexCoord = rotation * vTexCoord + vec2(0.5, 0.5);

  fTexCoord.x = clamp(fTexCoord.x, 0.0, 1.0);
  fTexCoord.y = clamp(fTexCoord.y, 0.0, 1.0);

  fAlpha = clamp(vLife, 0.0, 2.0) * 0.4;
}
