#version 330 core

in vec2 fTexCoord;
in float fAlpha;

out vec4 color;

uniform sampler2D TexData;

void main() {
  color = texture(TexData, fTexCoord);
  color.a *= fAlpha;
}
