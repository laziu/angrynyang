#pragma once
#include "Mesh.h"
#include "Texture.h"

class BlinderMesh: public Mesh
{
	std::shared_ptr<Texture> texture;

public:
	std::shared_ptr<Texture>& getTexture() { return texture; }

public:
	BlinderMesh(std::shared_ptr<Program>& program, std::shared_ptr<Texture>& texture):
		Mesh(std::shared_ptr<VAO>(new VAO(OBJ("resource/blinder", OBJ::type::bin), 0, 1, 2)), program), texture(texture) {
	}

public:
	void Bind(void) override {
		Mesh::Bind();
		glActiveTexture(GL_TEXTURE0);
		texture->Bind();
		glUniform1i(program->getUniformLoation("TexData"), 0);
	}
};

