#pragma once
#include <GL/glew.h>
#include "macro.h"

class FrameBuffer
{
public:
	GLuint id;

public:
	FrameBuffer(): id(0) {
		glGenFramebuffers(1, &id);
	}

	~FrameBuffer() {
		glDeleteFramebuffers(1, &id);
	}

	FORCED_MOVE_CONSTRUCTOR(FrameBuffer): id(o.id) {
		o.id = 0;
	}

	void Bind() {
		glBindBuffer(GL_FRAMEBUFFER, id);
	}
};

class DepthTexture
{
public:
	GLuint id;

public:
	DepthTexture(): id(0) {
		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, id, 0);

//		glDrawBuffer(GL_NONE);

		if ( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) 
			id = 0;
	}

	~DepthTexture() {
		glDeleteTextures(1, &id);
	}

	FORCED_MOVE_CONSTRUCTOR(DepthTexture): id(o.id) {
		o.id = 0;
	}

	void Bind() {
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, id);
		glUniform1i(id, 1);
	}
};