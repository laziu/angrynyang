#pragma once
#include <GL/glew.h>
#include <vector>
#include <map>
#include <memory>
#include <glm/glm.hpp>
#include "OBJ.h"
#include "macro.h"

class ArrayBuffer
{
protected:
	GLuint	id;
	GLint	attribSize;
	size_t	attribCount;

public:
	GLuint	getId         () const { return id;          }
	GLint	getAttribSize () const { return attribSize;  }
	size_t	getAttribCount() const { return attribCount; }

protected:
	ArrayBuffer() = delete;
	ArrayBuffer(GLint sz, size_t ct): attribSize(sz), attribCount(ct) {}

public:
	virtual ~ArrayBuffer() {
		if ( id )
			glDeleteBuffers(1, &id);
	}

protected:
	void genbuffer(GLenum target) {
		glGenBuffers(1, &id);
		glBindBuffer(target, id);
	}
};

class VBO: public ArrayBuffer
{
public:
	VBO(std::vector<GLfloat> const& data, GLint attributeSize, GLenum usage = GL_STATIC_DRAW)
	: ArrayBuffer(attributeSize, data.size() / attributeSize) {
		genbuffer(GL_ARRAY_BUFFER);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(GLfloat), data.data(), usage);
	}

	VBO(std::vector<glm::vec2> const& data, GLenum usage = GL_STATIC_DRAW): ArrayBuffer(2, data.size()) {
		genbuffer(GL_ARRAY_BUFFER);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec2), data.data(), usage);
	}

	VBO(std::vector<glm::vec3> const& data, GLenum usage = GL_STATIC_DRAW): ArrayBuffer(3, data.size()) {
		genbuffer(GL_ARRAY_BUFFER);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), data.data(), usage);
	}

	VBO(std::vector<glm::vec4> const& data, GLenum usage = GL_STATIC_DRAW): ArrayBuffer(4, data.size()) {
		genbuffer(GL_ARRAY_BUFFER);
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec4), data.data(), usage);
	}

	VBO(GLuint attributeCount, GLint attributeSize, GLenum usage = GL_STREAM_DRAW):
		ArrayBuffer(attributeSize, attributeCount) {
		genbuffer(GL_ARRAY_BUFFER);
		glBufferData(GL_ARRAY_BUFFER, attribCount * attribSize, NULL, usage);
	}

	FORCED_MOVE_CONSTRUCTOR(VBO): ArrayBuffer(o.attribSize, o.attribCount) {
		id = o.id;
		o.id = 0;
	}

public:
	// glBindBuffer VBO
	void Bind() { 
		glBindBuffer(GL_ARRAY_BUFFER, id); 
	}
};

class IBO: public ArrayBuffer
{
public:
	IBO(std::vector<GLuint> const& data, GLenum usage = GL_STATIC_DRAW): ArrayBuffer(1, data.size()) {
		genbuffer(GL_ELEMENT_ARRAY_BUFFER);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * sizeof(GLuint), data.data(), usage);
	}

	FORCED_MOVE_CONSTRUCTOR(IBO): ArrayBuffer(o.attribSize, o.attribCount) {
		id = o.id;
		o.id = 0;
	}

public:
	// glBindBuffer IBO
	void Bind() { 
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id); 
	}
};

class VAO
{
public:
	enum DrawMode
	{
#define GenEnum(name) name = GL_##name
		GenEnum(POINTS),
		GenEnum(LINES),
		GenEnum(LINES_ADJACENCY),
		GenEnum(LINE_STRIP),
		GenEnum(LINE_STRIP_ADJACENCY),
		GenEnum(LINE_LOOP),
		GenEnum(TRIANGLES),
		GenEnum(TRIANGLES_ADJACENCY),
		GenEnum(TRIANGLE_STRIP),
		GenEnum(TRIANGLE_STRIP_ADJACENCY),
		GenEnum(TRIANGLE_FAN),
		GenEnum(PATCHES)
#undef GenEnum
	};

private:
	GLuint		id;
	GLsizei		elementCount;
	DrawMode	drawMode;
	std::map<GLuint, std::shared_ptr<VBO>>	vboMap;
	std::shared_ptr<IBO>	ibo;
	bool iboInitialized;

public:
	GLuint  getId          () const { return id;           }
	GLsizei getElementCount() const { return elementCount; }
	GLenum  getDrawMode    () const { return drawMode;     }

public:
	VAO(DrawMode mode): elementCount(INT_MAX), drawMode(mode), iboInitialized(false) {
		glGenVertexArrays(1, &id);
	}

	VAO(OBJ const& obj, GLuint vertexLayout, GLuint texCoordLayout, GLuint normalLayout);

	virtual ~VAO();

	FORCED_MOVE_CONSTRUCTOR(VAO): VAO(TRIANGLES) {
		std::swap(id          , o.id          );
		std::swap(elementCount, o.elementCount);
		std::swap(drawMode    , o.drawMode    );
		std::swap(vboMap      , o.vboMap      );
		std::swap(ibo         , o.ibo         );
		std::swap(iboInitialized, o.iboInitialized);
	}

public:
	// glBindVertexArray VAO
	void Bind() { 
		glBindVertexArray(id); 
	}
	
	// Bind VBO to specified attribute index
	void SetVertexAttribPointer(std::shared_ptr<VBO> const& vbo, GLuint attribIndex);

	// Bind IBO 
	void SetVertexAttribPointer(std::shared_ptr<IBO> const& ibo);

	// glDrawElements VAO
	virtual void Draw() {
		if ( iboInitialized )
			glDrawElements(drawMode, ibo->getAttribCount(), GL_UNSIGNED_INT, 0);
		else
			glDrawArrays(drawMode, 0, elementCount);
	}
};
