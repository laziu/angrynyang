#pragma once
#include "Mesh.h"
#include "Texture.h"

class TvMesh: public Mesh
{
	std::shared_ptr<Texture> texture;

public:
	std::shared_ptr<Texture>& getTexture() { return texture; }

public:
	TvMesh(std::shared_ptr<Program>& program):
		Mesh(std::shared_ptr<VAO>(new VAO(OBJ("resource/tv_screen", OBJ::type::bin), 0, 1, -1)), program) {
		texture = std::shared_ptr<Texture>(new Texture(BMP("resource/screen_less.bmp")));
	}

	void Bind() override {
		Mesh::Bind();
		glActiveTexture(GL_TEXTURE6);
		texture->Bind();
		glUniform1i(program->getUniformLoation("TexData"), 6);
	}
};

