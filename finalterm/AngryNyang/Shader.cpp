#include "Shader.h"
#include <vector>
#include <iostream>

Shader::Shader(char const* code, GLenum type): id(0) {
	// create shader
	id = glCreateShader(type);
	glShaderSource(id, 1, &code, NULL);
	glCompileShader(id);

	// check if compile failed
	GLint isCompiled = 0;
	glGetShaderiv(id, GL_COMPILE_STATUS, &isCompiled);
	if ( isCompiled == GL_FALSE ) {
		GLint logLen = 0;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLen);
		std::vector<GLchar> log(logLen + 1);
		glGetShaderInfoLog(id, logLen, NULL, log.data());
		std::cerr << "--- Error: glShader is not compiled ------------" << std::endl
			      << log.data() << std::endl
			      << "------------------------------------------------" << std::endl;
		glDeleteShader(id);
		id = 0;
	}
}

Program::Program(VertShader const& vert, FragShader const& frag) {
	// create program
	id = glCreateProgram();
	glAttachShader(id, vert.getId());
	glAttachShader(id, frag.getId());
	glLinkProgram(id);

	// check if compile failed
	GLint isLinked = 0;
	glGetProgramiv(id, GL_LINK_STATUS, &isLinked);
	if ( isLinked == GL_FALSE ) {
		GLint logLen = 0;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logLen);
		std::vector<GLchar> log(logLen + 1);
		glGetProgramInfoLog(id, logLen, NULL, log.data());
		std::cerr << "--- Error: glProgram is not compiled -----------" << std::endl
				  << log.data() << std::endl
			      << "------------------------------------------------" << std::endl;
		glDeleteProgram(id);
		id = 0;
	}

	glDetachShader(id, vert.getId());
	glDetachShader(id, frag.getId());
}

