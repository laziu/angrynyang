#version 330 core

in vec3 fPosition;
in vec2 fTexCoord;
in vec3 fNormal;
in vec3 fCameraDir;
in vec3 fLightDir;

out vec4 color;

uniform sampler2D TexData;
uniform mat4 MV;
uniform vec3 LightPosition;
uniform float Alpha;
uniform float LightEnable;

void main() {
	if (LightEnable < 0.5) {
		color = texture(TexData, fTexCoord);
	} else {
		vec3 diffuse  = texture(TexData, fTexCoord).rgb;
		vec3 ambient  = vec3(0.1, 0.1, 0.1) * diffuse;
		vec3 specular = vec3(0.3, 0.3, 0.3);

		vec3 normal = normalize(fNormal);
		vec3 light  = normalize(fLightDir);
		vec3 ray    = normalize(fCameraDir);

		vec3 lightColor = vec3(1.0, 0.95, 0.9);
		float lightPower = 50.0;
		float lightDistance = length(LightPosition - fPosition);
		vec3 lightReflect = reflect(-light, normal);

		float cosTheta = clamp(dot(normal      , light), 0.0, 1.0);
		float cosAlpha = clamp(dot(lightReflect, ray  ), 0.0, 1.0);

		vec3 lightAmount = lightColor * lightPower / (lightDistance * lightDistance);

		color = vec4(ambient
		           + diffuse  * lightAmount * cosTheta
							 + specular * lightAmount * pow(cosAlpha, 5), Alpha);
	}
}
