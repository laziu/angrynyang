#version 330 core

in vec2 fTexCoord;

out vec3 color;

uniform sampler2D texData;

void main() {
  color = texture(texData, fTexCoord).rgb;
}
