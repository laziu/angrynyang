#pragma once
#include <GL/glew.h>
#include <string>
#include "macro.h"

class BMP
{
	unsigned char*	data;
	unsigned int	w, h;

public:
	unsigned char*	getData() const { return data; }
	unsigned int	getW   () const { return w;    }
	unsigned int	getH   () const { return h;    }

public:
	BMP(std::string const& imagePath): BMP(imagePath.c_str()) {}
	BMP(char const* imagePath);
	
	~BMP() { 
		if ( data )
			delete[] data; 
	}

	FORCED_MOVE_CONSTRUCTOR(BMP) {
		data = o.data;
		o.data = nullptr;
		w = o.w;
		h = o.h;
	}
};

class DDS
{
public:
	enum Type {
		DXT1 = 0x31545844U, // "DXT1" in ASCII
		DXT3 = 0x33545844U, // "DXT3" in ASCII
		DXT5 = 0x35545844U  // "DXT5" in ASCII
	};

private:
	unsigned char*	data;
	unsigned int	w, h;
	unsigned int	format;
	unsigned int	mipMapCount;

public:
	unsigned char*	getData  () const { return data;   }
	unsigned int	getW     () const { return w;      }
	unsigned int	getH     () const { return h;      }
	unsigned int	getFormat() const { return format; }
	unsigned int	getMipMapCount() const { return mipMapCount; }

public:
	DDS(std::string const& imagePath) : DDS(imagePath.c_str()) {}
	DDS(char const* imagePath);

	~DDS() {
		if ( data )
			delete[] data;
	}

	FORCED_MOVE_CONSTRUCTOR(DDS) {
		data = o.data;
		o.data = nullptr;
		w = o.w;
		h = o.h;
		format = o.format;
		mipMapCount = o.mipMapCount;
	}
};

class TGA
{
private:
	unsigned char*	data;
	unsigned char	imageTypeCode;
	unsigned short	w, h;
	unsigned char	bitCount;

public:
	unsigned char*	getData() const { return data; }
	unsigned short	getW   () const { return w;    }
	unsigned short	getH   () const { return h;    }
	unsigned int    getBit () const { return bitCount; }

public:
	TGA(std::string const& imagePath): TGA(imagePath.c_str()) {}
	TGA(char const* imagePath);

	~TGA() {
		if ( data )
			delete[] data;
	}

	FORCED_MOVE_CONSTRUCTOR(TGA) {
		data = o.data;
		o.data = nullptr;
		w = o.w;
		h = o.h;
		bitCount = o.bitCount;
		imageTypeCode = o.imageTypeCode;
	}
};

class Texture
{
	GLuint	id;

public:
	GLuint	getId() const { return id; }

public:
	Texture(BMP const& bmp);
	Texture(DDS const& dds);
	Texture(TGA const& tga);

	~Texture() {
		if ( id )
			glDeleteTextures(1, &id);
	}
	
	FORCED_MOVE_CONSTRUCTOR(Texture) {
		id = o.id;
		o.id = 0;
	}

public:
	// glBindTexture Texture2D
	void Bind() {
 		glBindTexture(GL_TEXTURE_2D, id);
	}
};

