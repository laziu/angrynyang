#pragma once
#include <GL/glew.h>
#include <string>
#include <utility>
#include "macro.h"

class Shader
{
protected:
	GLuint	id;

public:
	GLuint  getId() const { return id; }

protected:
	Shader(std::string const& code, GLenum type): Shader(code.c_str(), type) {}
	Shader(char const* code, GLenum type);
	Shader(): id(0) {}

public:
	virtual ~Shader() {
		if ( id )
			glDeleteShader(id);
	}
};

class VertShader: public Shader
{
public:
	VertShader(std::string const& code): VertShader(code.c_str()) {}
	VertShader(char const* code): Shader(code, GL_VERTEX_SHADER) {}

	FORCED_MOVE_CONSTRUCTOR(VertShader) {
		id = o.id;
		o.id = 0;
	}
};

class FragShader: public Shader
{
public:
	FragShader(std::string const& code): FragShader(code.c_str()) {}
	FragShader(char const* code): Shader(code, GL_FRAGMENT_SHADER) {}

	FORCED_MOVE_CONSTRUCTOR(FragShader) {
		id = o.id;
		o.id = 0;
	}
};


class Program
{
	GLuint	id;

public:
	GLuint  getId() const { return id; }

	GLuint getAttribLocation(std::string name) const {
		return glGetAttribLocation(id, name.c_str());
	}
	GLuint getUniformLoation(std::string name) const {
		return glGetUniformLocation(id, name.c_str());
	}

public:
	Program(VertShader const& vert, FragShader const& frag);
	Program(std::string const& vertCode, std::string const& fragCode):
		Program(VertShader(vertCode), FragShader(fragCode)) {}
	
	~Program(void) {
		if ( id )
			glDeleteProgram(id);
	}

	FORCED_MOVE_CONSTRUCTOR(Program) {
		id = o.id;
		o.id = 0;
	}

public:
	// glUseProgram Program
	void Use() { 
		glUseProgram(id); 
	}
};