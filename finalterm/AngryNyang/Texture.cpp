#pragma warning(disable:4996)
#include "Texture.h"
#include <cstdio>

BMP::BMP(char const* imagePath) {
	unsigned char header[54];
	unsigned int pos, size;

	FILE* file = fopen(imagePath, "rb");
	if ( !file ) {
		fprintf(stderr, "---BMP Error: %s is not exists. ----------\n", imagePath);
		return;
	}

#define GETINT(hex) (*(int*)&(header[hex]))

		// BMP has 54 bytes header
	if ( fread(header, 1, 54, file) != 54 ||
		// BMP file always begins with "BM"
		 header[0] != 'B' || header[1] != 'M' ||
		// Make sure the image is 24bpp
		 GETINT(0x1E) != 0 || GETINT(0x1C) != 24 ) {
		fprintf(stderr, "---BMP Error: %s is not a correct VMP file. ----------\n", imagePath);
		return;
	}

	// Read the information about the image
	pos  = GETINT(0x0A);
	size = GETINT(0x22);
	w    = GETINT(0x12);
	h    = GETINT(0x16);

#undef GETINT

	if ( size == 0 )
		size = w * h * 3;
	if ( pos = 0 )
		pos = 54;

	// Create buffer
	data = new unsigned char[size];

	// Read actual data from file
	fread(data, 1, size, file);

	// close 
	fclose(file);
}

DDS::DDS(char const * imagePath) {
	unsigned char header[124];
	unsigned int linearSize, fourCC;

	FILE *file = fopen(imagePath, "rb");
	if ( !file ) {
		fprintf(stderr, "---DDS Error: %s is not exists. ----------\n", imagePath);
		return;
	}

	char filecode[4];
	fread(filecode, 1, 4, file);
	if ( strncmp(filecode, "DDS ", 4) != 0 ) {
		fprintf(stderr, "---DDS Error: %s is not valid DDS. ----------\n", imagePath);
		fclose(file);
		return;
	}

	fread(&header, 124, 1, file);

#define GETUINT(hex) (*(unsigned int*)&(header[hex]))
	h           = GETUINT(8 );
	w           = GETUINT(12);
	linearSize  = GETUINT(16);
	mipMapCount = GETUINT(24);
	fourCC      = GETUINT(80);
#undef GETUINT

	unsigned int bufSize = mipMapCount > 1 ? linearSize * 2 : linearSize;
	data = new unsigned char[bufSize];
	fread(data, 1, bufSize, file);
	fclose(file);

	switch ( fourCC ) {
	case DXT1: format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT; break;
	case DXT3: format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; break;
	case DXT5: format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; break;
	default: free(data); return;
	}
}

TGA::TGA(char const * imagePath) {
	unsigned char ucharBad;
	short int sintBad;
	long imageSize;
	int colorMode;
	unsigned char colorSwap;

	FILE *file = fopen(imagePath, "rb");
	if ( !file ) {
		fprintf(stderr, "---DDS Error: %s is not exists. ----------\n", imagePath);
		return;
	}

	fread(&ucharBad, sizeof(unsigned char), 1, file);
	fread(&ucharBad, sizeof(unsigned char), 1, file);

	fread(&imageTypeCode, sizeof(unsigned char), 1, file);

	if ( imageTypeCode != 2 && imageTypeCode != 3 ) {
		fprintf(stderr, "---DDS Error: %s is not valid DDS. ----------\n", imagePath);
		fclose(file);
		return;
	}

	fread(&sintBad, sizeof(short int), 1, file);
	fread(&sintBad, sizeof(short int), 1, file);
	fread(&ucharBad, sizeof(unsigned char), 1, file);
	fread(&sintBad, sizeof(short int), 1, file);
	fread(&sintBad, sizeof(short int), 1, file);

	fread(&w, sizeof(short int), 1, file);
	fread(&h, sizeof(short int), 1, file);

	fread(&bitCount, sizeof(unsigned char), 1, file);

	fread(&ucharBad, sizeof(unsigned char), 1, file);

	colorMode = bitCount / 8;
	imageSize = w * h * colorMode;

	data = new unsigned char[imageSize];

	fread(data, sizeof(unsigned char), imageSize, file);
	/*
	for ( int i = 0; i < imageSize; i += colorMode ) {
		colorSwap = data[i];
		data[i] = data[i + 2];
		data[i + 2] = colorSwap;
	}
	*/
	fclose(file);
}

Texture::Texture(BMP const& bmp) {
	glGenTextures(1, &id);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.getW(), bmp.getH(), 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.getData());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
}

Texture::Texture(TGA const& tga) {
	glGenTextures(1, &id);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tga.getW(), tga.getH(), 0, GL_BGRA, GL_UNSIGNED_BYTE, tga.getData());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
}

Texture::Texture(DDS const & dds) {
	glGenTextures(1, &id);

	glBindTexture(GL_TEXTURE_2D, id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int width  = dds.getW();
	unsigned int height = dds.getH();
	unsigned int format = dds.getFormat();
	unsigned int mipMapCount = dds.getMipMapCount();
	unsigned char* data = dds.getData();

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT)? 8 : 16;
	unsigned int offset = 0;

	for ( unsigned int level = 0; level < mipMapCount && (width || height); ++level ) {
		unsigned int size = ((width + 3) / 4) * ((height + 3) / 4) * blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, 
							   width, height, 0, size, data + offset);
		offset += size;
		width /= 2;
		height /= 2;

		if ( width < 1 )
			width = 1;
		if ( height < 1 )
			height = 1;
	}
}
