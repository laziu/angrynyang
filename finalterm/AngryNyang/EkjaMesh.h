#pragma once
#include "Mesh.h"
#include "Texture.h"

class EkjaMesh: public Mesh
{
	std::shared_ptr<Texture> texture;

public:
	std::shared_ptr<Texture>& getTexture() { return texture; }

public:
	EkjaMesh(std::shared_ptr<Program>& program):
		Mesh(std::shared_ptr<VAO>(new VAO(OBJ("resource/ekja", OBJ::type::bin), 0, 1, 2)), program) {
		texture = std::shared_ptr<Texture>(new Texture(BMP("resource/bliss_less.bmp")));
	}
	
	void Bind() override {
		Mesh::Bind();
		glActiveTexture(GL_TEXTURE1);
		texture->Bind();
		glUniform1i(program->getUniformLoation("TexData"), 1);
	}
};

