#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

void Init(GLFWwindow* window);
void Destroy(GLFWwindow* window);
void Draw(GLFWwindow* window);
void Resize(GLFWwindow* window, int w, int h);
void KeyInput(GLFWwindow* window, int key, int scancode, int action, int mods);
void MouseButtonInput(GLFWwindow* window, int button, int action, int mods);