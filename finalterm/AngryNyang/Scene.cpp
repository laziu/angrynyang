#include "Scene.h"
#include "TestMesh.h"
#include "BlinderMesh.h"
#include "GlassMesh.h"
#include "EkjaMesh.h"
#include "SkyboxMesh.h"
#include "TvMesh.h"
#include "FireBallGenerator.h"
#include "DataIO.h"
#include "Boxes.h"
#include "Shadow.h"
#include <glm/gtc/matrix_transform.hpp>
namespace
{
	TestMesh* mesh;
	Mesh *skybox, *ekja, *tv, *blinder, *glass;
	Mesh* sphere;
	FireBallGenerator *fireball;
	Boxes room;
	Boxes::Box player;
	Boxes::Box initplayer;
	glm::mat4 mvp;
	Boxes::Box qbox;

//	FrameBuffer *framebuffer;
//	DepthTexture *depthtexture;
//	Program *depthprogram;
//	GLuint depthMatid;
//	GLuint depthBiasid, shadowMapid;

	std::vector<Boxes::Object> R;
	GLuint mvpid, vid, mid, lightid, alphaid, light_enableid;
//	GLuint skybox_mvpid, sphere_mvpid;
	glm::mat4 vMat, pMat;
	glm::vec3 initballpos(0.000000f, - 10.913523f, 0.000001f);

	int t_sw=0;
	double time, ttime[2];
	// initial position on +z
	glm::vec3 position(0.0f, 3.0f, 5.0f);
	glm::vec3 dir(1.0f, 0.0f, 0.0f);
	glm::vec3 up(0.0f, 1.0f, 0.0f);
	glm::vec3 velocity(0.0f, 0.0f, 0.0f);
	glm::vec3 g_acc(0.0f, -9.8f, 0.0f);
	glm::vec3 zero(0.0f, 0.0f, 0.0f);

	glm::vec3 dpos;// initial horizontal angle: toward -Z
	float hA = 3.14f;
	// initial vertical angle: 0
	float vA = 0;
	// initial field of view 
	float initialFoV = 45.0f;

	float mass = 2.0f;
	float speed = 7.0f;
	float mouseSpeed = 0.005f;
	float dT, dP;

	glm::vec3 box_normal[10];

	int box_face[10][5] ={
		{ 0,1,2,3 },{ 4,5,6,7 },{ 0,2,4,6 },{ 1,3,5,7 },{ 2,3,6,7 },{ 0,1,4,5 }
	};
}
int face_find(Boxes::Box p);
void ball_update(GLFWwindow* window);
int is_crash(Boxes::Box a, Boxes::Box p);
void jump();
void damp();
void move(Boxes::Box *x, glm::vec3 p);
void gravity();
bool crash_check(Boxes::Box x);
void calcMatrix(GLFWwindow* window);
void rander();
void init_play();
void set_box(Boxes::Box * bp, glm::vec3 pos);


void Init(GLFWwindow * window) {
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	mesh = new TestMesh;
	skybox = new SkyboxMesh(mesh->getProgram());
	ekja = new EkjaMesh(mesh->getProgram());
	tv = new TvMesh(mesh->getProgram());
	blinder = new BlinderMesh(mesh->getProgram(), mesh->getTexture());
	glass = new GlassMesh(mesh->getProgram(), mesh->getTexture());
	fireball = new FireBallGenerator;

//	framebuffer = new FrameBuffer;
//	depthtexture = new DepthTexture;
//	depthprogram = new Program(IO::ReadString("glsl/DepthTest.vert"), IO::ReadString("glsl/DepthTest.frag"));

	ttime[0] = ttime[1] = 0.0;

	player = player_init();
	initplayer = player_init();

	room.Load("resource/interior-colider.boxes");

	R= room.object;

	box_normal[0] = glm::vec3(-1.0f,0.0f,0.0f);
	box_normal[1] = glm::vec3(1.0f, 0.0f, 0.0f);
	box_normal[2] = glm::vec3(0.0f, -1.0f, 0.0f);
	box_normal[3] = glm::vec3(0.0f, 1.0f, 0.0f);
	box_normal[4] = glm::vec3(0.0f, 0.0f, -1.0f);
	box_normal[5] = glm::vec3(0.0f, 0.0f, 1.0f);
	
	mvpid = mesh->getProgram()->getUniformLoation("MVP");
	vid   = mesh->getProgram()->getUniformLoation("V");
	mid   = mesh->getProgram()->getUniformLoation("M");
	lightid = mesh->getProgram()->getUniformLoation("LightPosition");
	alphaid = mesh->getProgram()->getUniformLoation("Alpha");
	light_enableid = mesh->getProgram()->getUniformLoation("LightEnable");

//	skybox_mvpid = skybox->getProgram()->getUniformLoation("MVP");

//	depthMatid = depthprogram->getUniformLoation("depthMVP");
//	depthBiasid = mesh->getProgram()->getUniformLoation("DepthBiasMVP");
//	shadowMapid = mesh->getProgram()->getUniformLoation("shadowMap");
}

void Destroy(GLFWwindow * window) {
	delete mesh;
	delete skybox;
	delete ekja;
	delete tv;
	delete blinder;
	delete glass;
	delete fireball;
//	delete framebuffer;
//	delete depthtexture;
}

glm::mat4 mMat;
glm::vec3 lightPos(4, 4, 4);
glm::mat4 depthMVP = glm::mat4(1.0f);

void Draw(GLFWwindow * window) {
	using namespace glm;
	/*
	// generate shadow
	framebuffer->Bind();
	glViewport(0, 0, 1024, 1024);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	depthprogram->Use();

	glm::mat4 depthProjMat = glm::perspective(45.0f, 1.0f, 2.0f, 50.0f);
	glm::mat4 depthViewMat = glm::lookAt(lightPos, glm::vec3(0,0,0), glm::vec3(0, 1, 0));
	depthMVP     = depthProjMat * depthViewMat * glm::mat4(1.0f);

	glUniformMatrix4fv(depthMatid, 1, GL_FALSE, &depthMVP[0][0]);
	
	mesh->getVAO()->Bind();
	mesh->Draw();

	ekja->getVAO()->Bind();
	ekja->Draw();

	blinder->getVAO()->Bind();
	blinder->Draw();

	glass->getVAO()->Bind();
	glass->Draw();
	
	// render with shadow
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 1024, 768);
	*/
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	calcMatrix(window);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mesh->Bind();

	mMat = glm::mat4(1.0f);
	mvp = pMat * vMat * mMat;

//	glm::mat4 biasMat(
//		0.5, 0.0, 0.0, 0.0,
//		0.0, 0.5, 0.0, 0.0,
//		0.0, 0.0, 0.5, 0.0,
//		0.5, 0.5, 0.5, 1.0
//	);
//	glm::mat4 depthBiasMVP = biasMat * depthMVP;

	glUniformMatrix4fv(mvpid, 1, GL_FALSE, &mvp[0][0]);
	glUniformMatrix4fv(vid  , 1, GL_FALSE, &vMat[0][0]);
	glUniformMatrix4fv(mid  , 1, GL_FALSE, &mMat[0][0]);
//	glUniformMatrix4fv(depthBiasid, 1, GL_FALSE, &depthBiasMVP[0][0]);

//	depthtexture->Bind();

	glUniform3f(lightid, lightPos.x, lightPos.y, lightPos.z);
	glUniform1f(alphaid, 1.0f);
	glUniform1f(light_enableid, 1.0f);

	mesh->Draw();

	ekja->Bind();
	ekja->Draw();

	blinder->Bind();
	glUniform1f(alphaid, 0.8f);
	blinder->Draw();

	glass->Bind();
	glUniform1f(alphaid, 0.3f);
	glass->Draw();
	
	tv->Bind();
	glUniform1f(alphaid, 1.0f);
	glUniform1f(light_enableid, 0.0f);
	tv->Draw();

	skybox->Bind();
	mMat = translate(rotate(mat4(1.0f), (float)time * 0.01f, vec3(0, 1, 0)), position);
	mvp = pMat * vMat * mMat;
	glUniformMatrix4fv(mvpid, 1, GL_FALSE, &mvp[0][0]);
	glUniformMatrix4fv(mid, 1, GL_FALSE, &mMat[0][0]);
	skybox->Draw();

	ball_update(window);
	mesh->Bind();
	fireball->Update(dT, position, dir);
	fireball->Draw(vMat, pMat);
}

struct ball
{
	int sw_ball=0;
	glm::vec3 ball_pos, ball_dir;
	Boxes::Box bx;
	double KT;
};
ball S[10];

int b_cnt=0;
double LBT=0.0;

void set_box(Boxes::Box * bp,glm::vec3 pos) {
	int i;
	float xm, xM, ym, yM, zm, zM;
	xm = pos.x - 0.3; xM = pos.x + 0.3;
	ym = pos.y - 0.3; yM = pos.y + 0.3;
	zm = pos.z - 0.3; zM = pos.z + 0.3;
	(*bp).point[0] = glm::vec3(xm, ym, zM);
	(*bp).point[1] = glm::vec3(xm, yM, zM);
	(*bp).point[2] = glm::vec3(xm, ym, zm);
	(*bp).point[3] = glm::vec3(xm, yM, zm);
	(*bp).point[4] = glm::vec3(xM, ym, zM);
	(*bp).point[5] = glm::vec3(xM, yM, zM);
	(*bp).point[6] = glm::vec3(xM, ym, zm);
	(*bp).point[7] = glm::vec3(xM, yM, zm);
}

void ball_update(GLFWwindow * window) {
	int i;

	if ( time-LBT>0.5 && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && b_cnt <6 ) {
		for ( i = 0; i < 6; i++ ) if ( S[i].sw_ball == 0 ) break;
		S[i].sw_ball = 1;
		S[i].ball_dir = dir*2.5f*dT;
		S[i].ball_pos = position + dir;
		set_box(&S[i].bx, position);

		S[i].KT = time;
		b_cnt++;
		LBT = time;

		fireball->fireballs[i].position = S[i].ball_pos;
		fireball->fireballs[i].life = true;
	}
	int j;

	for ( i = 0; i < 6; i++ ) {
		if ( S[i].sw_ball>0 ) {
			S[i].ball_pos += S[i].ball_dir*400.0f*dT;
			move(&S[i].bx, S[i].ball_dir*400.0f*dT);
			fireball->fireballs[i].position = S[i].ball_pos;

			if ( crash_check(S[i].bx) ) {
				S[i].ball_pos -= S[i].ball_dir*400.0f*dT;
				move(&S[i].bx, -S[i].ball_dir*400.0f*dT);
				while ( 1 ) {
					move(&S[i].bx, S[i].ball_dir*0.001f);
					j=face_find(S[i].bx);
					if ( j < 6 ) break;
				}

				glm::vec3 ndir=box_normal[j] * glm::dot(-box_normal[j], S[i].ball_dir);
				S[i].ball_dir += ndir * 2.0f;

				if ( S[i].sw_ball == 4 ) {
					S[i].sw_ball = 0;
					b_cnt--;
				}
				else S[i].sw_ball ++;
			}
			if ( time - S[i].KT > 10.0 ) {
				S[i].sw_ball = 0;
				b_cnt--;
			}
		}
		else
			fireball->fireballs[i].life = false;
	}
}

ball S2[10];

int b_cnt2 = 0;
double LBT2 = 0.0;

int face_find(Boxes::Box p) {
	int k=0;
	float axm = p.point[0].x, axM = p.point[4].x, aym = p.point[0].y, ayM = p.point[1].y, azm = p.point[2].z, azM = p.point[0].z;
	float pxm = qbox.point[0].x, pxM = qbox.point[4].x, pym = qbox.point[0].y, pyM = qbox.point[1].y, pzm = qbox.point[2].z, pzM = qbox.point[0].z;

	if ( (ayM < pym || pyM < aym) ) {
		k += 1;
	}
	if ( (axM < pxm || pxM < axm) ) {
		k += 2;
	}
	if ( (azM < pzm || pzM < azm) ) {
		k += 4;
	}
	if ( k == 1 ) {
		if ( ayM < pym ) {
			return 2;
		}
		else {
			return 3;
		}
	}
	else if ( k == 2 ) {
		if ( axM < pxm ) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else if ( k == 4 ) {
		if ( azM < pzm ) {
			return 5;
		}
		else {
			return 4;

		}
	}
	return 6;
}


void Resize(GLFWwindow * window, int w, int h) {
	float ratio = w / (float)h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-ratio, ratio, -1.0f, 1.0f, 1.0f, -1.0f);
	glMatrixMode(GL_PROJECTION);
}

void KeyInput(GLFWwindow * window, int key, int scancode, int action, int mods) {
	if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS )
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void MouseButtonInput(GLFWwindow* window, int button, int action, int mods) {
	if ( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS ) {
		if ( fireball ) {
			//fireball->AddElement(position + dir * 3.0f, dir);
		}
	}
}

bool jumping = false;

void calcMatrix(GLFWwindow* window) {
	static double lastTime = glfwGetTime();

	ttime[t_sw] = glfwGetTime();
	dP = float(ttime[t_sw] - ttime[!t_sw]);
	t_sw = !t_sw;




	time = glfwGetTime();
	dT = float(time - lastTime);

	// get mouse position
	double x, y;
	glfwGetCursorPos(window, &x, &y);

	// reset mouse position for next frame
	glfwSetCursorPos(window, 1024/2, 768/2);

	// compute new orientation
	hA += mouseSpeed * float(1024/2 - x);
	vA += mouseSpeed * float( 768/2 - y);

	// direction: spherical coordinates to cartesian coordinates conversion
	dir = glm::vec3(
		cos(vA) * sin(hA),
		sin(vA),
		cos(vA) * cos(hA)
	);

	// right vector
	glm::vec3 right(
		sin(hA - 3.14f/2.0f),
		0,
		cos(hA - 3.14f/2.0f)
	);

	// up vector
	up = glm::cross(right, dir);

	dpos = zero;
	// move forward
	if ( glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS )
		dpos += dir * dT * speed;	
	if ( glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS )
		dpos -= dir * dT * speed;
	if ( glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS )
		dpos += right * dT * speed;
	if ( glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS )
		dpos -= right * dT * speed;
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		jump();
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		init_play();
//		dpos += up * dT * speed;
//	if ( glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS )
//		dpos -= up * dT * speed;

	gravity();

	dpos.y = 0.0f;
	move(&player,dpos);
	if (crash_check(player)) {
		move(&player,-dpos);
		damp();
	}

	float xD, yM, zD;
	xD = (player.point[0].x + player.point[4].x) / 2.0f;
	yM = player.point[1].y;
	zD = (player.point[0].z + player.point[2].z) / 2.0f;
	position = glm::vec3(xD, yM, zD);


	pMat = glm::perspective(initialFoV, 4.0f/3.0f, 0.1f, 200.0f);
	vMat = glm::lookAt( position, position + dir, up );

	lastTime = time;
}

void init_play()
{
	for (int i = 0; i < 8; i++) player.point[i] = initplayer.point[i];
	velocity = zero;
}

void rander()
{
	float axm = player.point[0].x, axM = player.point[4].x, aym = player.point[0].y, ayM = player.point[1].y, azm = player.point[2].z, azM = player.point[0].z;
	glBegin(GL_QUADS);
		glVertex3f(axm, aym, azm);
		glVertex3f(axm, ayM, azm);
		glVertex3f(axM, ayM, azm);
		glVertex3f(axM, aym, azm);
		
		glVertex3f(axm, aym, azM);
		glVertex3f(axm, ayM, azM);
		glVertex3f(axM, ayM, azM);
		glVertex3f(axM, aym, azM);
	glEnd();

}

void damp()
{
	if(velocity.y>0.0f) velocity += g_acc*(0.00017f)*60.0f*dT;
	else	velocity += -g_acc*(0.00017f)*60.0f*dT;
}

int jump_buffer = 0,max_jump=50;
double JT = 0.0; int  jbt = 500;
void jump()
{
//	if (jump_buffer<max_jump && (time-JT)/dP>=jbt) {
	if ( !jumping ) {
		velocity = -g_acc*0.01f;
		move(&player, velocity * dT * 60.0f);
//		jump_buffer++;
		jumping = true;
	}
//	else {
//		if(jump_buffer<= max_jump) JT = time;
//		jump_buffer=0;
//		if (jump_buffer < 0) jump_buffer = 0;
//	}
}

void move(Boxes::Box *x, glm::vec3 p)
{
	for (int i = 0; i < 8; i++) (*x).point[i] += p;
}

void gravity() 
{
	move(&player,velocity * dT * 60.0f);
	if (crash_check(player)) {
		move(&player,-velocity * dT * 60.0f);
		if ( velocity.y < 0 )
		jumping = false;
		velocity = zero;
	}else {
		velocity += g_acc*(0.0002f) * dT * 60.0f;
	}
}
std::vector<Boxes::Object>::iterator boit;
std::vector<Boxes::Box>::iterator bbit;
std::array<glm::vec3, 8>::iterator bait;

bool crash_check(Boxes::Box x)
{

	for (boit = R.begin(); boit != R.end(); ++boit) {
		Boxes::Object Ri = (*boit);

		std::vector<Boxes::Box> p = Ri.box;
		for (bbit = p.begin(); bbit != p.end(); ++bbit) {
			Boxes::Box k = (*bbit);
			int q = is_crash(k, x);
			if (q) {
				qbox = k;
				return 1;
			}
		}
	}
	return 0;
}

int is_crash(Boxes::Box a, Boxes::Box p)
{
	float axm = a.point[0].x, axM = a.point[4].x, aym = a.point[0].y, ayM = a.point[1].y, azm = a.point[2].z, azM = a.point[0].z;
	float pxm = p.point[0].x, pxM = p.point[4].x, pym = p.point[0].y, pyM = p.point[1].y, pzm = p.point[2].z, pzM = p.point[0].z;

	if (!(ayM < pym || pyM < aym) && !(axM < pxm || pxM < axm) && !(azM < pzm || pzM < azm)) return 1;

	return 0;
}