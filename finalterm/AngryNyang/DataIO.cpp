#include "DataIO.h"
#include <fstream>
#include <iostream>

std::string IO::ReadString(const std::string& fileName) {
	std::ifstream file(fileName);
	std::string content;

	if ( !file )
		std::cout << "--- ERROR : " << fileName << " is not exist." << std::endl;
	else
		content = std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

	file.close();
	return content;
}

bool IO::WriteString(const std::string & fileName, const std::string & content) {
	std::ofstream file(fileName);

	if ( !file ) {
		std::cout << "--- ERROR : " << fileName << " is not exist." << std::endl;
		return false;
	}
	else
		file << content;

	file.close();
	return true;
}
