#pragma once
#include "Mesh.h"
#include <glm/glm.hpp>
#include "Texture.h"

class TestMesh: public Mesh
{
	std::shared_ptr<Texture> texture;

public:
	std::shared_ptr<Texture>& getTexture() { return texture; }

public:
	TestMesh();

public:
	void Bind(void) override;
};

