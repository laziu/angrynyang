#pragma once

// Delete copy operator, and make move operator
#define FORCED_MOVE_CONSTRUCTOR(class_name) \
	class_name(class_name const&) = delete; \
	class_name& operator=(class_name const&) = delete; \
	class_name(class_name&& o)
