#pragma warning(disable:4996)
#include "Boxes.h"
#include <cstdio>

void Boxes::Load(std::string filename) {
	if ( initialized )
		return;

	FILE *in = fopen(filename.c_str(), "r");
	if ( !in ) {
		fprintf(stderr, "-- Error: %s is not exists.\n", filename.c_str());
		return;
	}

	char buffer[1000];
	int n;

	while ( fscanf(in, "%s", buffer) == 1 && strcmp(buffer, "o") == 0 ) {
		fscanf(in, "%s %d", buffer, &n);
		
		Object o;
		o.name = std::string(buffer);
		
		for ( ; n > 0; --n ) {
			Box b;
			for ( int i = 0; i < 8; ++i )
				fscanf(in, "%f %f %f", &b.point[i].x, &b.point[i].y, &b.point[i].z);
			o.box.push_back(b);
		}

		object.push_back(o);
	}

	initialized = true;
}

Boxes::Box player_init()
{
	Boxes::Box p;
	float xm, xM, ym, yM, zm, zM;
	xm = -0.5f; xM = 0.5f;
	ym = 1.0f; yM = 2.0f;
	zm = 4.5f; zM = 5.5f;
	p.point[0] = glm::vec3(xm, ym, zM);
	p.point[1] = glm::vec3(xm, yM, zM);
	p.point[2] = glm::vec3(xm, ym, zM);
	p.point[3] = glm::vec3(xm, yM, zM);
	p.point[4] = glm::vec3(xM, ym, zM);
	p.point[5] = glm::vec3(xM, yM, zM);
	p.point[6] = glm::vec3(xM, ym, zM);
	p.point[7] = glm::vec3(xM, yM, zM);
	return p;
}

void Boxes::Unload() {
	if ( !initialized )
		return;

	object.clear();

	initialized = false;
}


// Test usage of Boxes
// class Boxes contains many Object
// class Boxes::Object contains name and many Box
// class Boxes::Box contains 8 points
// coordination of point[i] -> ( i & 1, (i & 2) >> 1, (i & 4) >> 2 ) 
int test_boxes() {
	Boxes b("resource/interior-colider.boxes");
	
	for ( auto const& i : b.object ) {
		
		printf("%s\n", i.name.c_str());
		
		for ( auto const& j : i.box ) {
			for ( auto const& k : j.point )
				printf("%f %f %f\n", k.x, k.y, k.z);
			printf("\n");
		}
		
		printf("\n");
	}

	getchar();
	return 0;
}