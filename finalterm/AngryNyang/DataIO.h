#pragma once
#pragma warning(disable:4996)
#include <string>
#include <vector>
#include <fstream>
#include <cstdio>

class IO
{
public:
	// Read whole text file
	static std::string ReadString(std::string const& fileName);
	// Write text file. It could overwrite existing file.
	static bool WriteString(std::string const& fileName, std::string const& content);

	// Read binary file
	template<typename T>
	static std::vector<T> ReadBinary(std::string const& filename);
	// Write binary file. It could overwrite existing file.
	template<typename T>
	static bool WriteBinary(std::string const& fileName, std::vector<T> const& content);

	// Check file exist
	static bool FileExists(std::string const& filename) {
		return std::ifstream(filename).good();
	}
};



template<typename T>
inline std::vector<T> IO::ReadBinary(std::string const & filename) {
	FILE *fp;

	if ( (fp = fopen(filename.c_str(), "rb")) == NULL )
		return std::vector<T>();

	fseek(fp, 0, SEEK_END);
	long size = ftell(fp) / sizeof(T);
	fseek(fp, 0, SEEK_SET);

	if ( size <= 0 ) {
		fclose(fp);
		return std::vector<T>();
	}

	std::vector<T> v(size);
	
	if ( fread(v.data(), sizeof(T), size, fp) != size ) {
		fclose(fp);
		return std::vector<T>();
	}

	fclose(fp);

	return v;
}

template<typename T>
inline bool IO::WriteBinary(std::string const & fileName, std::vector<T> const & content) {
	FILE *fp;

	if ( (fp = fopen(fileName.c_str(), "wb")) == NULL )
		return false;

	fwrite(content.data(), sizeof(T), content.size(), fp);

	fclose(fp);

	return true;
}
