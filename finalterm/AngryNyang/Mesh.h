#pragma once
#include "VertexObject.h"
#include "Shader.h"

class Mesh
{
protected:
	std::shared_ptr<VAO    > vao;
	std::shared_ptr<Program> program;

public:
	std::shared_ptr<VAO    >& getVAO    () { return vao;     }
	std::shared_ptr<Program>& getProgram() { return program; }

public:
	Mesh(std::shared_ptr<VAO    >& vao,
		 std::shared_ptr<Program>& program)
		 : vao(vao), program(program) {}
	virtual ~Mesh() {}

	FORCED_MOVE_CONSTRUCTOR(Mesh):
		vao(std::move(o.vao)),
		program(std::move(o.program)) {
	}

protected:
	Mesh() {}

public:
	// use program, bind vao
	virtual void Bind(void) {
		program->Use();
		vao->Bind();
	}

	virtual void Draw(void) {
		vao->Draw();
	}
};

