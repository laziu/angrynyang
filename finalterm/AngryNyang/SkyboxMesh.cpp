#include "SkyboxMesh.h"
#include "OBJ.h"
#include "DataIO.h"

using namespace std;

SkyboxMesh::SkyboxMesh(std::shared_ptr<Program>& prog) {
	// load obj
	if ( false ) {
		OBJ obj("resource/skybox.obj");
		obj.Serialize("resource/skybox");
	}

	OBJ obj("resource/skybox", OBJ::type::bin);

	vao = shared_ptr<VAO>(new VAO(obj, 0, 1, -1));
	
	program = prog;

	texture = shared_ptr<Texture>(new Texture(BMP("resource/skybox_less.bmp")));
}

void SkyboxMesh::Bind() {
	Mesh::Bind();
	glActiveTexture(GL_TEXTURE4);
	texture->Bind();
	glUniform1i(program->getUniformLoation("TexData"), 4);
}

