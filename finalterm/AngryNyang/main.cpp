#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstdio>
#include "OBJ.h"
#include "DataIO.h"

#include "Scene.h"

GLFWwindow* window;

int main(void) {
	
	glfwSetErrorCallback([](int error, char const* description) {
		fprintf(stderr, "GLFW error %d: %s\n", error, description);
	}); 

	// initialize GLFW
	if ( !glfwInit() ) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}
	
	//Set the GLFW window creation hints - these are optional
//	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); //Request a specific OpenGL version
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
//	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	// create a window and its opengl context
	window = glfwCreateWindow(1024, 768, "AngryNyang", nullptr, nullptr);
	if ( !window ) {
		fprintf(stderr, "Failed to open GLFW window. Might be not Opengl 3.3 compatible.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(window);

	// initialize glew
	{
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();
		if ( err != GLEW_OK ) {
			fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
			getchar();
			glfwTerminate();
			return -1;
		}
	}


	// set callback
	glfwSetWindowSizeCallback(window, Resize);
	glfwSetKeyCallback(window, KeyInput);
	glfwSetMouseButtonCallback(window, MouseButtonInput);

	// set input mode
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	
	// set the cursor at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024/2, 768/2);

	// main process of program
	Init(window);
	do {
		Draw(window);
		glfwSwapBuffers(window);
		glfwPollEvents();
	} while ( !glfwWindowShouldClose(window) );
	Destroy(window);

	// destroy GLFW window
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
