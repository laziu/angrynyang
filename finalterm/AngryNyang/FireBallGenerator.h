#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <array>
#include <algorithm>
#include <memory>
#include "Shader.h"
#include "VertexObject.h"
#include "Texture.h"

struct Particle
{
	glm::vec3 position, velocity;
	float size;
	float life;
	float cameradistance;

	Particle(): life(-1.0f), cameradistance(-1.0f) {}

	bool operator<(Particle const& o) const {
		return this->cameradistance > o.cameradistance;
	}
};

struct FireBall
{
	static unsigned int const ContainerSize = 1000;
	std::array<Particle, ContainerSize> particles;
	int lastUsedIndex = 0;

	glm::vec3 position;
	float spread = 0.5f;
	bool life;
	
	FireBall(): position(0), life(false) {}
	FireBall(glm::vec3 position, glm::vec3 velocity): 
		position(position), life(true) {}

	void UpdateParticles(float dT, glm::vec3 cameraPosition, glm::vec3 cameraDirection);
};


class FireBallGenerator
{
public:
	static unsigned int const ContainerSize = 10000;

	std::shared_ptr<VAO> particleVao;
	std::shared_ptr<VBO> particlePosVbo, particleAlphaVbo;
	std::shared_ptr<Program> particleProgram;
	std::shared_ptr<Texture> particleTex;

	std::array<FireBall, 10> fireballs;
	std::array<glm::vec4, ContainerSize> particle_xyzs_buffer;
	std::array<GLfloat  , ContainerSize> particle_alpha_buffer;

	GLuint cameraRightId, cameraUpId, vpId;

public:
	FireBallGenerator();

	void Update(float dT, glm::vec3 cameraPosition, glm::vec3 cameraDirection);
	void Draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
};

